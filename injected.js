// Отменяем возможность закрывать shadow root
Element.prototype._attachShadow = Element.prototype.attachShadow;
Element.prototype.attachShadow = function () {
    return this._attachShadow( { mode: "open" } );
};

// Функция, предназначенная для получения всех клиентов из api
async function getAjxLogins(){
    let response = await fetch('https://sys.mrtruman.ru/ads/api/screen/rsya/get/login/9bcf974d9bca3c7f390bf7004755a425');

    let json = await response.json();
    const list = document.getElementsByClassName('mrtruman_push_div')[0];
	list.classList.add("active");
    list.innerHTML = '';

    // Заполняем интерфейс относительно ответа
    for (const el of json) {
        const link = document.createElement('a')
        link.innerHTML = el['name'];
        link.onclick = () => {getAjxCampaings(el['id'])}
        list.appendChild(link)
    }

}

// Функция, предназначенная для получения рекламных компаний определенного клиента
async function getAjxCampaings(login){
    let response = await fetch('https://sys.mrtruman.ru/ads/api/screen/rsya/get/'+login+'/9bcf974d9bca3c7f390bf7004755a425');
    let json = await response.json();
    const list = document.getElementsByClassName('mrtruman_push_div')[0]
    list.innerHTML = ''

    // Заполняем интерфейс относительно ответа
    for (const el of json) {
        const link = document.createElement('a')
        link.innerHTML = el['name']+" "+el['id'];
        link.onclick = () => {getAjaxAd(login, el['id'])}
        list.appendChild(link)
    }
}

// Функция, предназначенная для получения определенной рекламы, на которую и будет происходить замена
async function getAjaxAd(login, ad_id) {
    let response = await fetch('https://sys.mrtruman.ru/ads/api/screen/rsya/get/text/' + login + '/' + ad_id + '/9bcf974d9bca3c7f390bf7004755a425');
    let json = await response.json();
    const list = document.getElementsByClassName('mrtruman_push_div')[0];
    list.innerHTML = '';
	list.classList.remove("active");

    // Запускаем функцию, которая подменит оригинальные рекламы на рекламу из api
	for (const el of json) {
		setAd(el);
		return;
    }
    
}

// Функция, предназначенная для поиска на странице реклам. Поиск реклам происходит на основе слова 'реклама', которое содержится в объявлении
function find_ads() {
    // Ищем все shadow root, но сначала обнуляем список, дабы избежать повторного занесения данных
    shadow_roots = []
    update_shadow_roots(document.body)

    // Итерируем по всем элементам из тела страницы и из всех shadow root
    const ads = []
    for (const block of [document.querySelectorAll('*')].concat(shadow_roots.map(item => item.querySelectorAll('*')))) {
        for (const el of block) {
            // Получаем все before и after элементы, так как слово 'реклама' может содержаться и в них
            const before = window.getComputedStyle(el, ':before')
            const before_second = window.getComputedStyle(el, '::before')
            const after = window.getComputedStyle(el, ':after')
            const after_second = window.getComputedStyle(el, '::after')

            // Получаем весь текст элемента, исключая текст из дочерних элементов
            const el_text =
                ([...el.childNodes].map(item => {
                        if (item.nodeType === 3) return item.nodeValue
                    }).join(' ') +
                    (before.content !== 'none' ? before.content : '') +
                    (after.content !== 'none' ? after.content : '') +
                    (before_second.content !== 'none' ? before_second.content : '') +
                    (after_second.content !== 'none' ? after_second.content : '')).toLowerCase()
            // Если же в тексте элемента содержится слово 'реклама', то это и есть объявление
            if (
                el_text.includes('реклама') &&
                el_text !== 'реклама на сайте' && (
                    !el.parentElement.parentElement ||
                    !el.parentElement.parentElement.getElementsByTagName('iframe').length)
            ) {
                // Необходимо найти обертку самой рекламы
                let i = 0
                var parent = el.parentElement.parentElement.parentElement
                while (i <= 6) {
                    if (!parent) break

                    // Если же блок содержит картинку или видео, то это и есть обертка рекламы
                    if (([...parent.querySelectorAll('img')].filter(img => img.width >= 128).length ||
                        [...get_background_images(parent)].filter(img => img.offsetWidth >= 128).length ||
                        parent.querySelectorAll('video').length) &&
                        (parent.offsetWidth <= (window.innerWidth * .8) || parent.offsetHeight < window.innerHeight)
                    ) {
                            ads.push(parent)
                            // console.log(parent, el, el_text)
                            break
                    }
                    parent = parent.parentElement
                    i++
                }
                break
            }
        }
    }

    return ads
}

// Проверяет, содержит ли элемент только внутренний текст и элементы span. При этом span так же должен содержать только текст или другие span
function contains_only_text(item) {
    return [...item.childNodes].filter(child => {
        // console.log(child, child.nodeType, child.tagName)
        return child.nodeType === 3 || (child.nodeType === 1 && child.tagName.toLowerCase() === 'span' && contains_only_text(child))
    }).length === item.childNodes.length
}

// Поиск всех shadow root
function update_shadow_roots(el) {
    // Если элемент содержит shadow root, то его необходимо добавить в массив
    if (el.shadowRoot) {
        shadow_roots.push(el.shadowRoot)
    }
    // Далее итерируем по дочерних элементам, дабы получить shadow root и из них
    for (const child of el.childNodes) {
        if (child.assignedNodes && child.assignedNodes()[0]){
            update_shadow_roots(child.assignedNodes()[0])
        } else { update_shadow_roots(child)}
    }
}

// Получение всех фоновых картинок родительского и дочерних элементов
function get_background_images(el) {
    return [...el.querySelectorAll("div[style*='background-image']")].concat([...el.querySelectorAll("div[background-image]")])
}

// Обновляет все рекламы на сайте на новую
function setAd(new_ad_info) {
    // console.log(new_ad_info)
    // Поиск реклам
    const ads = find_ads()
    // console.log(ads, 'ADS')

    // Итерируем по рекламам
    for (const ad of ads) {
        const images = ad.querySelectorAll('img')
        const background_images = get_background_images(ad)
        // const videos = ad.querySelectorAll('video')

        // Ставим обводку для рекламы
        if (images.length || background_images.length) ad.style.border = '1px solid red';

        let is_favicon_updated = false
        let is_img_updated = false
        // console.log(ad, images, background_images)

        // Далее заменяем основную и favicon-картинки на новыео
        for (const img of images) {
            if (img.width > 0 && img.width <= 128 && !is_favicon_updated) {
                // console.log('entered', img['src'], new_ad_info['favicon'])
                img['src'] = new_ad_info['favicon'];
                is_favicon_updated = true;
            }
            if (img.width > 128 && !is_img_updated) {
                img['src'] = new_ad_info['img']
                // img.style.height = 'auto'
                img.style.top = '50%'
                img.style.transform = 'translateY(-50%)'
                is_img_updated = true
            }
        }
        // console.log(ad, background_images)
        for (const background_img of background_images) {
            // console.log(background_img, background_img.offsetWidth, is_favicon_updated)
            if (background_img.offsetWidth > 0 && background_img.offsetWidth <= 128 && !is_favicon_updated) {
                background_img.style.backgroundImage = 'url(' + new_ad_info['favicon'] + ')';
                background_img.style.backgroundSize = 'cover'
                background_img.style.backgroundPosition = 'center center';
                is_favicon_updated = true
            }
            if (background_img.offsetWidth > 128 && !is_img_updated) {
                // console.log(background_img, new_ad_info['img'])
                background_img.style.backgroundImage = 'url(' + new_ad_info['img'] + ')';
                background_img.style.backgroundSize = 'cover'
                background_img.style.backgroundPosition = 'center center';
                background_img.style.setProperty = (a, b) => {}
                is_img_updated = true
            }
        }
        // if (videos.length) {
        //     const div = document.createElement('div')
        //     div.style = videos[0].style
        //     div.style.backgroundRepeat = 'no-repeat';
        //     div.style.backgroundOrigin = 'content-box';
        //     div.style.backgroundImage = 'url(' + img + ')'
        //     div.style.width = '100%';
        //     div.style.height = '100%';
        //     videos[0].parentNode.replaceChild(div, videos[0])
        // }

        // Заменяем текст на нужный
        let is_title_placed = false
        let is_text_placed = false
        // Итерируем по всем элементам рекламы
        for (const el of ad.querySelectorAll('*')) {
            if (el.tagName.toLowerCase() !== 'style') {
                const texts = []
                const spans = []
                // Ищем все текста и span у элемента
                for (const child of el.childNodes) {
                    // console.log(child)
                    if (child.nodeType === 3) texts.push(child)
                    else if (child.nodeType === 1 && child.tagName.toLowerCase() === 'span' && contains_only_text(child)) spans.push(child)
                }

                // Продолжаем, если текст был найден
                if (texts.length) {
                    // получаем текст в сыром виде
                    const el_text = texts.map(item => item.nodeValue).concat(spans.map(item => item.textContent)).join('').toLowerCase().trim()
                    // Проверяем на наличие русских символов
                    const m = el_text.match(/[а-я]/g)
                    // Проверяем на наличие латинских символов
                    const eng_m = el_text.match(/[a-z]/g)

                    // Продолжаем, если текст содержит русские символы
                    if (m && m.length &&
                        !el_text.includes('реклама') && 
                        el_text !== 'узнать больше' &&
                        el_text !== 'подробнее' &&
                        el_text !== 'рейтинг орзанизации' &&
                        el_text !== 'перейти на сайт') {
                        // console.log('russian text'  , el_text, is_title_placed, is_text_placed, el)

                        // Продолжаем, если заголовок не был еще размещен
                        if (!is_title_placed) {
                            texts.forEach(item => item.nodeValue = '')
                            spans.forEach(item => item.innerHTML = '')
                            texts[0].nodeValue = new_ad_info['title']
                            is_title_placed = true
                        }
                        // Продолжаем, если заголовок уже размещен, а текст еще нет
                        else if (!is_text_placed) {
                            texts.forEach(item => item.nodeValue = '')
                            spans.forEach(item => item.innerHTML = '')
                            texts[0].nodeValue = new_ad_info['text']
                            is_text_placed = true
                        }
                        // Убираем лишний текст
                        else {
                            texts.forEach(item => item.nodeValue = '')
                            spans.forEach(item => item.innerHTML = '')
                            texts[0].nodeValue = ''
                        }
                    }
                    // Продолжаем, если текст содержит английские символы. Основная идея заключается в том, что домен сайта зачастую подписывается только английскими символами
                    else if ((!m || !m.length) && (eng_m && eng_m.length)) {
                        // console.log('placed', l[0], l[0].nodeValue, new_ad_info['domain'], el)
                        texts.forEach(item => item.nodeValue = '')
                        spans.forEach(item => item.innerHTML = '')
                        texts[0].nodeValue = new_ad_info['domain']
                    }
                } 
            }
        }
    }
}

// Объявляем пустой массив, дабы далее заполнить его shadow root
var shadow_roots = []
