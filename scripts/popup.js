// Функция, предназначенная для инициализации плагина
function initApp(){
	// Добавляет div, в котором будет весь интерфейс
    $('body').prepend("<div class='mrtruman_push_div'></div>");

	// добавляем стили
	var path = chrome.runtime.getURL('mrtruman.css');
	$('head').append($('<link>')
    .attr("rel","stylesheet")
    .attr("type","text/css")
    .attr("href", path));
	// Ставим слушатель, при произведении которого будет открываться интерфейс
	let timerId = setInterval(function(){
		if(document.screen === 1){
			 const list = document.getElementsByClassName('mrtruman_push_div')[0];
			 list.classList.add("active");
			 list.innerHTML = '';
			$('.mrtruman_push_div').prepend("<a OnClick='getAjxLogins();'>GO</a>");
			clearInterval(timerId);
		}
	}, 1000);
}

// Добавляем скрипт на главную страницу
const s = document.createElement('script');
s.src = chrome.runtime.getURL('injected.js');
s.onload = function() {
    this.remove();
};

const el = (document.head || document.documentElement)
el.insertBefore(s, el.firstChild);

window.onload = initApp
